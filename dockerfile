FROM node:14

WORKDIR /usr/src/app

#COPIAR DA PASTA LOCAL
COPY . .

#Instala dependências
RUN npm install

#Define a porta a ser exposta para rodar a aplicação
EXPOSE 3000

#O docker entende que o comando tem que executar quando sobe o container
CMD [ "npm", "start" ]